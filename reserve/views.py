from django.contrib.auth.decorators import permission_required
from django.contrib.auth.mixins import PermissionRequiredMixin
from django.db import transaction, IntegrityError
from django.http import Http404
from django.shortcuts import render
from django.urls import reverse_lazy
from django.views import generic

from main.models import Facility, Reservation, Staff

@permission_required('main.add_reservation', 'login', raise_exception=True)
@transaction.atomic
def reserve_index(request):
    title = "Add Reservation"
    reservation = Reservation.objects.none()
    if request.method == 'POST':
        reservation_form = ReservationForm(request.POST)
        if reservation_form.is_valid():
            try:
                with transaction.atomic():
                    customer = Customer(
                        first_name=reservation_form.cleaned_data.get('first_name'),
                        middle_name=reservation_form.cleaned_data.get('middle_name'),
                        last_name=reservation_form.cleaned_data.get('last_name'),
                        email_address=reservation_form.cleaned_data.get('email'),
                        contact_no=reservation_form.cleaned_data.get('contact_no'),
                        address=reservation_form.cleaned_data.get('address'),
                    )
                    customer.save()
                    staff = request.user
                    reservation = Reservation(
                        staff=get_object_or_404(Staff, user=staff),
                        customer=customer,
                        no_of_children=reservation_form.cleaned_data.get('no_of_children'),
                        no_of_adults=reservation_form.cleaned_data.get('no_of_adults'),
                        expected_arrival_date_time=reservation_form.cleaned_data.get('expected_arrival_date_time'),
                        expected_departure_date_time=reservation_form.cleaned_data.get('expected_departure_date_time'),
                        reservation_date_time=timezone.now(),
                    )
                    reservation.save()
                    for room in reservation_form.cleaned_data.get('rooms'):
                        room.reservation = reservation
                        room.save()
            except IntegrityError:
                raise Http404
            return render(
                request,
                'reserve_success.html', {
                    'reservation': reservation,
                }
            )
    else:
        reservation_form = ReservationForm()

    return render(
        request,
        'reserve.html', {
            'title': title,
            'reservation_form': reservation_form,
        }
    )


def reserve_success(request):
    pass